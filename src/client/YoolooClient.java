// History of Change
// vernr    |date  | who | lineno | what
//  V0.106  |200107| cic |    -   | add  start_Client() SERVERMESSAGE_CHANGE_STATE 

package client;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import common.LoginMessage;
import common.YoolooKarte;
import common.YoolooKartenspiel;
import common.YoolooSpieler;
import common.YoolooStich;
import messages.ClientMessage;
import messages.ClientMessage.ClientMessageType;
import messages.ServerMessage;

public class YoolooClient {

	protected String serverHostname = "localhost";
	protected int serverPort = 44137;
	protected Socket serverSocket = null;
	protected ObjectInputStream ois = null;
	protected ObjectOutputStream oos = null;

	protected ClientState clientState = ClientState.CLIENTSTATE_NULL;

	//private String spielerName = "Name" + (System.currentTimeMillis() + "").substring(6);
	private String spielerName = randomname() ;
	private LoginMessage newLogin = null;
	protected YoolooSpieler meinSpieler;
	protected YoolooStich[] spielVerlauf = null;

	public YoolooClient() {
		super();
	}

	public YoolooClient(String serverHostname, int serverPort) {
		super();
		this.serverPort = serverPort;
		clientState = ClientState.CLIENTSTATE_NULL;
	}

	/**
	 * Client arbeitet statusorientiert als Kommandoempfaenger in einer Schleife.
	 * Diese terminiert wenn das Spiel oder die Verbindung beendet wird.
	 */
	public void startClient() {

		try {
			clientState = ClientState.CLIENTSTATE_CONNECT;
			verbindeZumServer();

			while (clientState != ClientState.CLIENTSTATE_DISCONNECTED && ois != null && oos != null) {
				
				// 1. Schritt Kommado empfangen
				ServerMessage kommandoMessage = empfangeKommando();
				System.out.println("[id-x]ClientStatus: " + clientState + "] " + kommandoMessage.toString());
				
				// 2. Schritt ClientState ggfs aktualisieren (fuer alle neuen Kommandos)
				ClientState newClientState = kommandoMessage.getNextClientState();
				if (newClientState != null) {
					clientState = newClientState;
				}
				
				// 3. Schritt Kommandospezifisch reagieren
				
				switch (kommandoMessage.getServerMessageType()) {
				case SERVERMESSAGE_SENDLOGIN:
					// Server fordert Useridentifikation an
					// Falls User local noch nicht bekannt wird er bestimmt
					if (newLogin == null || clientState == ClientState.CLIENTSTATE_LOGIN) {
						// TODO Klasse LoginMessage erweiteren um Interaktives ermitteln des
						// Spielernames, GameModes, ...)
						newLogin = eingabeSpielerDatenFuerLogin(); //Dummy aufruf
						try {
							anzeige(spielerName);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						newLogin = new LoginMessage(spielerName);
					}
					// Client meldet den Spieler an den Server
					oos.writeObject(newLogin);
					System.out.println("[id-x]ClientStatus: " + clientState + "] : LoginMessage fuer  " + spielerName
							+ " an server gesendet   auf Spielerdaten");
					empfangeSpieler();
					// ausgabeKartenSet();
					break;
				case SERVERMESSAGE_SORT_CARD_SET:
					// sortieren Karten
					meinSpieler.sortierungFestlegen();
					ausgabeKartenSet();
					// ggfs. Spielverlauf löschen
					spielVerlauf = new YoolooStich[YoolooKartenspiel.maxKartenWert];
					ClientMessage message = new ClientMessage(ClientMessageType.ClientMessage_OK,
							"Kartensortierung ist erfolgt!");
					try {
						speicherKarten(meinSpieler);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					oos.writeObject(message);
					break;
				case SERVERMESSAGE_SEND_CARD:
					spieleStich(kommandoMessage.getParamInt());
					break;
				case SERVERMESSAGE_RESULT_SET:
					System.out.println("[id-" + meinSpieler.getClientHandlerId() + "]ClientStatus: " + clientState
							+ "] : Ergebnis ausgeben ");
					String ergebnis = empfangeErgebnis();
					System.out.println(ergebnis.toString());
					break;
					           // basic version: wechsel zu ClientState Disconnected thread beenden
				case SERVERMESSAGE_CHANGE_STATE:
				break ;
				case SERVERMESSAGE_WrongLogin:
					System.out.println("Der Name ist leider schon vergeben w�hle bitte einen anderen");
					break;
				default:
					break;
				}
			}
			//TODO: Stats  Speichern
			
			
			try {
				speichern(meinSpieler.getName(), false);
				
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Verbindung zum Server aufbauen, wenn Server nicht antwortet nach ein Sekunde
	 * nochmals versuchen
	 *
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	// TODO Abbruch nach x Minuten einrichten
	protected void verbindeZumServer() throws UnknownHostException, IOException {
		while (serverSocket == null) {
			try {
				serverSocket = new Socket(serverHostname, serverPort);
			} catch (ConnectException e) {
				System.out.println("Server antwortet nicht - ggfs. neu starten");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
				}
			}
		}
		System.out.println("[Client] Serversocket eingerichtet: " + serverSocket.toString());
		// Kommunikationskanuele einrichten
		ois = new ObjectInputStream(serverSocket.getInputStream());
		oos = new ObjectOutputStream(serverSocket.getOutputStream());
	}

	protected void spieleStich(int stichNummer) throws IOException {
		System.out.println("[id-" + meinSpieler.getClientHandlerId() + "]ClientStatus: " + clientState
				+ "] : Spiele Karte " + stichNummer);
		spieleKarteAus(stichNummer);
		YoolooStich iStich = empfangeStich();
		spielVerlauf[stichNummer] = iStich;
		System.out.println("[id-" + meinSpieler.getClientHandlerId() + "]ClientStatus: " + clientState
				+ "] : Empfange Stich " + iStich);
		if (iStich.getSpielerNummer() == meinSpieler.getClientHandlerId()) {
			System.out.print(
					"[id-" + meinSpieler.getClientHandlerId() + "]ClientStatus: " + clientState + "] : Gewonnen - ");
			meinSpieler.erhaeltPunkte(iStich.getStichNummer() + 1);
		}

	}

	protected void spieleKarteAus(int i) throws IOException {
		oos.writeObject(meinSpieler.getAktuelleSortierung()[i]);
	}

	// Methoden fuer Datenempfang vom Server / ClientHandler
	protected ServerMessage empfangeKommando() {
		ServerMessage kommando = null;
		boolean failed = false;
		try {
			kommando = (ServerMessage) ois.readObject();
		} catch (ClassNotFoundException e) {
			failed = true;
			e.printStackTrace();
		} catch (IOException e) {
			failed = true;
			e.printStackTrace();
		}
		if (failed)
			kommando = null;
		return kommando;
	}

	protected void empfangeSpieler() {
		try {
			meinSpieler = (YoolooSpieler) ois.readObject();
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
	}

	protected YoolooStich empfangeStich() {
		try {
			return (YoolooStich) ois.readObject();
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	protected String empfangeErgebnis() {
		try {
			return (String) ois.readObject();
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	protected LoginMessage eingabeSpielerDatenFuerLogin() {
		// TODO Spielername, GameMode und ggfs mehr ermitteln
		return null;
	}

	public void ausgabeKartenSet() {
		// Ausgabe Kartenset
		System.out.println("[id-" + meinSpieler.getClientHandlerId() + "]ClientStatus: " + clientState
				+ "] : Uebermittelte Kartensortierung beim Login ");
		for (int i = 0; i < meinSpieler.getAktuelleSortierung().length; i++) {
			System.out.println("[id-" + meinSpieler.getClientHandlerId() + "]ClientStatus: " + clientState
					+ "] : Karte " + (i + 1) + ":" + meinSpieler.getAktuelleSortierung()[i]);
		}

	}
	
	protected void sendLogin() throws IOException {
		if (newLogin == null || clientState == ClientState.CLIENTSTATE_LOGIN) {
			
			// TODO Klasse LoginMessage erweiteren um Interaktives ermitteln des
			// Spielernames, GameModes, ...)
			newLogin = eingabeSpielerDatenFuerLogin(); //Dummy aufruf
			newLogin = new LoginMessage(spielerName);
		}
		
		// Client meldet den Spieler an den Server
		oos.writeObject(newLogin);
		
		System.out.println("[id-x]ClientStatus: " + clientState + "] : LoginMessage fuer  " + spielerName
				+ " an server gesendet warte auf Spielerdaten");
		empfangeSpieler();
		// ausgabeKartenSet();
	}
	
	protected void sortCardSet() throws IOException{
		meinSpieler.sortierungFestlegen();
		ausgabeKartenSet();
		
		// ggfs. Spielverlauf loeschen
		spielVerlauf = new YoolooStich[YoolooKartenspiel.maxKartenWert];
		ClientMessage message = new ClientMessage(ClientMessageType.ClientMessage_OK,
				"Kartensortierung ist erfolgt!");
		oos.writeObject(message);
	}

	public enum ClientState {
		CLIENTSTATE_NULL, // Status nicht definiert
		CLIENTSTATE_CONNECT, // Verbindung zum Server wird aufgebaut
		CLIENTSTATE_LOGIN, // Anmeldung am Client Informationen des Users sammeln
		CLIENTSTATE_RECEIVE_CARDS, // Anmeldung am Server
		CLIENTSTATE_SORT_CARDS, // Anmeldung am Server
		CLIENTSTATE_REGISTER, // t.b.d.
		CLIENTSTATE_PLAY_SINGLE_GAME, // Spielmodus einfaches Spiel
		CLIENTSTATE_DISCONNECT, // Verbindung soll getrennt werden
		CLIENTSTATE_DISCONNECTED // Vebindung wurde getrennt
	};
	
	
	//XXX
	public static void speicherKarten (YoolooSpieler meinSpieler) throws Exception {
		
		YoolooKarte[] karten=meinSpieler.getAktuelleSortierung();
		JSONObject spieler = new JSONObject();
		String name = meinSpieler.getName();
		File f = new File("NutzerKarten/"+name+".json");
		if(f.exists() && !f.isDirectory()){ 
			

			spieler.get("Gespielte_Karten::");
		    
		}
		 JSONArray letzteKarten = new JSONArray();
		 for (int i= 0 ; i<meinSpieler.getAktuelleSortierung().length;i++) {
				letzteKarten.add(karten[i].getWert());
				}
	
		 spieler.put("Gespielte_Karten:", letzteKarten);
		  
		Files.write(Paths.get("NutzerKarten/"+name+".json"), spieler.toJSONString().getBytes());
	}
	
	public void anzeige(String name) throws Exception {
		JSONObject spieler = new JSONObject();
		File f = new File("NutzerKarten/"+name+".json");
		if(f.exists() && !f.isDirectory()){ 
			spieler = (JSONObject) jsonEinlesen(f);
			
			
			System.out.println("Zuletzt gespielte Kartenreihenfolge:"+ spieler.get("Gespielte_Karten:"));
		}
	
	}
	
	public static void speichern(String name,boolean spielGewonnen) throws Exception {
		JSONObject spieler = new JSONObject();
		
		long spiele=0;
		long gewonnen=0;
		long verloren=0;
		
		
		
		
		File f = new File("Nutzer/"+name+".json");
		if(f.exists() && !f.isDirectory()){ 
			
			spieler = (JSONObject) jsonEinlesen(f);
		    spiele = (long) spieler.get("Spiele:");
		    gewonnen = (long) spieler.get("Anzahl Gewonnen:");
		    verloren = (long) spieler.get("Anzahl Verloren:");
		   
		}
		    
		   
		if(spielGewonnen==true){ 
			gewonnen++;
		}else {
			verloren++;
		}
		
		
		spiele++;
		spieler.put("Name:", name);
	    spieler.put("Spiele:",spiele );
	    spieler.put("Anzahl Gewonnen:", gewonnen);
	    spieler.put("Anzahl Verloren:", verloren);
	   
	    
	    Files.write(Paths.get("Nutzer/"+name+".json"), spieler.toJSONString().getBytes());
	    
		}

	public static Object jsonEinlesen(File f) throws Exception {
	    FileReader reader = new FileReader(f);
	    JSONParser jsonParser = new JSONParser();
	    return jsonParser.parse(reader);
	}
	
public String randomname() {
	String name = "test";
	
	int random= (int) (Math.random() * 10);
	System.out.println(random);
	switch(random) {
	case 1: name ="Noel2";break;
	case 2: name ="Ksenia";break;
	case 3: name ="Yannik";break;
	case 4: name ="Dimitri";break;
	case 5: name = "Spieler";break;
	case 6: name ="Noel2";break;
	case 7: name ="Ksenia2";break;
	case 8: name ="Yannik2";break;
	case 9: name ="Dimitri2";break;
	case 10: name = "Tester2";break;
	default: name= "test";break;
	}
	return name;
}
}


package BotAlgorithmus;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import client.YoolooClient;
import common.YoolooKarte;
import common.YoolooKartenspiel;
import common.YoolooStich;
import messages.ClientMessage;
import messages.ServerMessage;
import messages.ClientMessage.ClientMessageType;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Bot extends YoolooClient {

	public Bot() {
		super();
	}

	public Bot(String hostname, int port) {
		super(hostname, port);
	}

	@Override
	/**
	 * Client arbeitet statusorientiert als Kommandoempfaenger in einer Schleife.
	 * Diese terminiert wenn das Spiel oder die Verbindung beendet wird.
	 */
	public void startClient() {

		try {
			clientState = ClientState.CLIENTSTATE_CONNECT;
			verbindeZumServer();

			while (clientState != ClientState.CLIENTSTATE_DISCONNECTED && ois != null && oos != null) {
				// 1. Schritt Kommado empfangen
				ServerMessage kommandoMessage = empfangeKommando();
				System.out.println("[id-x]ClientStatus: " + clientState + "] " + kommandoMessage.toString());
				// 2. Schritt ClientState ggfs aktualisieren (fuer alle neuen Kommandos)
				ClientState newClientState = kommandoMessage.getNextClientState();
				if (newClientState != null) {
					clientState = newClientState;
				}
				// 3. Schritt Kommandospezifisch reagieren
				switch (kommandoMessage.getServerMessageType()) {
				case SERVERMESSAGE_SENDLOGIN:
					sendLogin();
					break;
				case SERVERMESSAGE_SORT_CARD_SET:
					// sortieren Karten
					sortCardSet();
					break;
				case SERVERMESSAGE_SEND_CARD:
					spieleStich(kommandoMessage.getParamInt());
					break;
				case SERVERMESSAGE_RESULT_SET:
					System.out.println("[id-" + meinSpieler.getClientHandlerId() + "]ClientStatus: " + clientState
							+ "] : Ergebnis ausgeben ");
					String ergebnis = empfangeErgebnis();
					System.out.println(ergebnis.toString());
					break;
				// basic version: wechsel zu ClientState Disconnected thread beenden
				case SERVERMESSAGE_CHANGE_STATE:

					System.out.println("---- Das Spiel ist vorbei ----");
					System.out.println("Ich bin " + meinSpieler.getSpielfarbe() + "| ALLE Spiele: ");
					int indexI = 0;
					Integer kartenFuerNaechstesSpiel[] = new Integer[10];
					for (YoolooStich stich : spielVerlauf) {

						System.out.println(Arrays.toString(stich.getStich()));

						for (YoolooKarte karte : stich.getStich()) {

							int aktuellerWert = karte.getWert();

							if (kartenFuerNaechstesSpiel[indexI] == null) {
								if (karte.getFarbe() != meinSpieler.getSpielfarbe()) {
									kartenFuerNaechstesSpiel[indexI] = aktuellerWert;
								}
							}

							else if (kartenFuerNaechstesSpiel[indexI] < aktuellerWert) {
								if (karte.getFarbe() != meinSpieler.getSpielfarbe()) {
									kartenFuerNaechstesSpiel[indexI] = aktuellerWert;
								}
							}
						}
						indexI++;
					}

					JSONArray siegerKarten = new JSONArray();
					for (Integer karte : kartenFuerNaechstesSpiel) {

						int siegerKarte = karte;
						siegerKarten.add(siegerKarte);
						System.out.println(karte.toString());
					}

					try {

						FileWriter file = new FileWriter("botJSON/siegerKarten.json");
						file.write(siegerKarten.toJSONString());
						file.flush();
						file.close();

					} catch (IOException e) {
						e.printStackTrace();
					}

					break;

				default:
					break;
				}
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void sortCardSet() throws IOException {
		JSONParser parser = new JSONParser();
		try {
			FileReader reader = new FileReader("botJSON/siegerKarten.json");
			Object jsonObj = parser.parse(reader);
			JSONArray jsonArray = (JSONArray) jsonObj;

			int anzKarten = meinSpieler.getAktuelleSortierung().length;

			if (jsonArray.size() > anzKarten || jsonArray.size() < anzKarten) {
				meinSpieler.sortierungFestlegen();
			} else {

				YoolooKarte[] neueSortierung = new YoolooKarte[anzKarten];
				YoolooKarte[] verfuegbareKarten = meinSpieler.getAktuelleSortierung();
				List<Integer> verfuegbareWerte = new ArrayList<Integer>();
				List<Integer> verfuegbareSiegerKartenWerte = new ArrayList<Integer>();

				for (int i = 0; i < jsonArray.size(); i++) {

					verfuegbareSiegerKartenWerte.add(((Number) jsonArray.get(i)).intValue());

				}

				for (YoolooKarte karte : verfuegbareKarten) {
					verfuegbareWerte.add(karte.getWert());
				}

				int i = 0;
				while (verfuegbareWerte.size() > 0) {

					int letzteSiegerKarteSize = verfuegbareSiegerKartenWerte.size() - 1;
					int letzteSiegerKarte = verfuegbareSiegerKartenWerte.get(letzteSiegerKarteSize);
					int[] kartenGroesser = groeßerAls_InArray(verfuegbareWerte, letzteSiegerKarte);

					int neuerKartenWert;
					if (kartenGroesser.length > 0) {
						neuerKartenWert = Arrays.stream(kartenGroesser).min().getAsInt();
					} else {
						neuerKartenWert = Collections.min(verfuegbareWerte);
					}
					verfuegbareWerte.remove(verfuegbareWerte.indexOf(neuerKartenWert));

					System.out.printf("Neue Karte an Position %d ist %d", i, neuerKartenWert);

					neueSortierung[i] = new YoolooKarte(meinSpieler.getSpielfarbe(), neuerKartenWert);
					verfuegbareSiegerKartenWerte.remove(letzteSiegerKarteSize);
					i++;
				}

				meinSpieler.setAktuelleSortierung(neueSortierung);
				System.out.println(meinSpieler.getAktuelleSortierung().toString());

			}
		} catch (FileNotFoundException | ParseException e) {
			// e.printStackTrace();
			System.out.println("RIP");
			meinSpieler.sortierungFestlegen();
		}

		ausgabeKartenSet();

		// ggfs. Spielverlauf loeschen
		spielVerlauf = new YoolooStich[YoolooKartenspiel.maxKartenWert];
		ClientMessage message = new ClientMessage(ClientMessageType.ClientMessage_OK, "Kartensortierung ist erfolgt!");
		oos.writeObject(message);

	}

	private int[] groeßerAls_InArray(List<Integer> verfuegbareWerte, int N) {
		List<Integer> groeßerAls = new ArrayList<Integer>();

		for (Integer wert : verfuegbareWerte) {
			if (wert > N)
				groeßerAls.add(wert);
		}

		return groeßerAls.stream().mapToInt(Integer::intValue).toArray();
	}
}
